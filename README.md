# dotfiles
This project installs my personal dotfiles and other helpful resources.

## Features
Ansible is used to configure the local machine with **calcurse**, **feh**,
**git**, **mpv**, **mutt**, **newsboat**, **ranger**, **screenfetch**,
**tmux**, **vim**, **zathura**, and their respective configuration files.

1. Install packages
2. Configure vim
    - Set up a .vimrc file
    - Set up a .vim directory with color profiles

## Installation
Run bootstrap.sh to start the install process.

```shell
./bootstrap.sh
```
