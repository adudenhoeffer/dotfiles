#!/usr/bin/env bash
# bootstrap.sh

HOSTS=hosts
PLAYBOOK=playbook.yml

# Print a welcome message.
echo "[BOOTSTRAP] Setting up the local workstation..."
echo

# Install ansible.
echo "[BOOTSTRAP] Installing ansible..."
if [ -n "$(command -v apt)" ]
then
    sudo apt -q -y install ansible
elif [ -n "$(command -v dnf)" ]
then
    sudo dnf -q -y install ansible
else
    echo "[BOOTSTRAP] Unable to download ansible. Exiting..."
    exit -1
fi
echo

# Run the ansible playbook.
echo "[BOOTSTRAP] Running ansible playbook..."
ansible-playbook -i $HOSTS $PLAYBOOK --ask-become-pass

# Print a goodbye message.
echo "[BOOTSTRAP] Finished setting up the local workstation."
echo
